use std::{
    collections::HashMap,
    fs::File,
    io::{Read, Seek},
    mem::{size_of, transmute, MaybeUninit},
    path::Path,
};

#[repr(packed)]
#[derive(Debug, Clone, Copy)]
#[allow(unused)]
pub struct WadHead {
    magic: [u8; 4],
    num_entries: i32,
    dir_offset: i32,
}

#[repr(i8)]
#[derive(Debug, Clone, Copy, PartialEq)]
#[allow(unused)]
pub enum WadEntryType {
    Palette = 0x40,
    StatusPic = 0x42,
    MipTexture = 0x44,
    ConsolePicture = 0x45,
}

#[repr(packed)]
#[derive(Debug, Clone, Copy)]
pub struct WadEntry {
    pub offset: i32,
    pub dsize: i32,
    pub size: i32,
    pub wtype: WadEntryType,
    pub comprs: i8,
    pub dummy: u16,
    pub name: [u8; 16],
}

#[derive(Debug, Clone)]
pub struct WadFlat {
    pub width: u32,
    pub height: u32,
    pub pixels: Vec<u8>,
}

#[repr(packed)]
#[derive(Debug, Clone, Copy)]
#[allow(unused)]
struct WadMiptextureHeader {
    pub name: [u8; 16],
    pub width: u32,
    pub height: u32,
    pub scale_offset: [u32; 4],
}

const PALETTE: &'static [u8] = include_bytes!("../palette.lmp");

#[derive(Debug, Clone)]
pub struct WadMipTexture {
    pub name: String,
    pub width: u32,
    pub height: u32,
    pub scale_pixels: [Vec<u8>; 4],
}

impl WadMipTexture {
    pub fn to_truecolor(&self, index: u32) -> Vec<u8> {
        self.scale_pixels[index as usize]
            .iter()
            .flat_map(|index| {
                let index = *index as u32;
                [
                    PALETTE[(index * 3) as usize],
                    PALETTE[(index * 3 + 1) as usize],
                    PALETTE[(index * 3 + 2) as usize],
                ]
            })
            .collect()
    }
}

#[allow(unused)]
pub struct WadPalette {
    colors: [(u8, u8, u8); 256],
}

macro_rules! read_struct {
    ($r:expr, $st:ty) => {
        (|| {
            let mut data: MaybeUninit<[u8; size_of::<$st>()]> = MaybeUninit::uninit();
            unsafe {
                $r.read_exact(&mut *data.as_mut_ptr())?;
            }
            Ok::<_, std::io::Error>(unsafe { std::mem::transmute::<_, $st>(data.assume_init()) })
        })()
    };
}

fn bytes_to_str(bytes: &[u8]) -> String {
    let terminator = bytes.iter().position(|v| *v == 0);
    let bytes = terminator
        .map(|terminator| &bytes[0..terminator])
        .unwrap_or(bytes);
    String::from_utf8_lossy(bytes).into_owned()
}

#[derive(Debug, Clone)]
#[allow(unused)]
pub struct Wad {
    head: WadHead,
    dirs: Vec<WadEntry>,
    images: HashMap<String, WadMipTexture>,
}

impl Wad {
    pub fn load_from_path(path: impl AsRef<Path>) -> std::io::Result<Self> {
        let mut file = File::open(path)?;
        Self::load(&mut file)
    }

    pub fn load<R: Read + Seek>(r: &mut R) -> std::io::Result<Self> {
        let mut head_buf = [0u8; size_of::<WadHead>()];
        r.read_exact(&mut head_buf)?;
        let head: WadHead = unsafe { transmute(head_buf) };
        let mut dirs = Vec::with_capacity(head.num_entries as _);
        r.seek(std::io::SeekFrom::Start(head.dir_offset as _))?;
        for _ in 0..head.num_entries {
            let dir = read_struct!(r, WadEntry)?;
            dirs.push(dir);
        }

        let mut images = HashMap::new();
        for dir in dirs.iter() {
            if dir.wtype == WadEntryType::MipTexture {
                images.insert(bytes_to_str(&dir.name), Self::load_image(&dir, r)?);
            }
        }

        Ok(Self { head, dirs, images })
    }

    pub fn mip_texture(&self, name: &String) -> Option<&WadMipTexture> {
        self.images.get(name)
    }

    pub fn mip_textures(&self) -> &HashMap<String, WadMipTexture> {
        &self.images
    }

    fn load_image<R: Read + Seek>(dir: &WadEntry, r: &mut R) -> std::io::Result<WadMipTexture> {
        r.seek(std::io::SeekFrom::Start(dir.offset as _))?;
        let npixels = (dir.size - 24) as _;
        let mut pixels = Vec::with_capacity(npixels);
        pixels.resize(npixels, 0);
        let header = read_struct!(r, WadMiptextureHeader)?;

        let mut scale_pixels: [Vec<u8>; 4] = (0..4)
            .map(|mip| {
                let w = header.width;
                let h = header.height;
                let mip_scale = 1 << mip;
                let mip_pixel_count = (w / mip_scale) * (h / mip_scale);
                let mut pixels = Vec::with_capacity(mip_pixel_count as _);
                pixels.resize(mip_pixel_count as _, 0u8);
                pixels
            })
            .collect::<Vec<_>>()
            .try_into()
            .unwrap();

        for pixels in scale_pixels.iter_mut() {
            r.read_exact(pixels)?;
        }

        let mip_texture = WadMipTexture {
            name: bytes_to_str(&header.name),
            width: header.width,
            height: header.height,
            scale_pixels,
        };

        Ok(mip_texture)
    }
}

#[cfg(test)]
pub mod tests {
    use std::io::Cursor;

    use super::Wad;

    #[test]
    pub fn test() {
        let mut stream = Cursor::new(include_bytes!("../test.wad"));
        let wad = Wad::load(&mut stream).unwrap();
        println!("{wad:?}");
    }
}
