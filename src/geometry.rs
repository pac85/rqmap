use std::{collections::HashMap, io::Write, iter::repeat};

use crate::{
    math::Vec3,
    parser::{Brush, Plane},
    wad::Wad,
};

fn planes_common_point(planes: [&Plane; 3]) -> Option<Vec3<f64>> {
    let [(norm0, dist0), (norm1, dist1), (norm2, dist2)] = planes.map(|p| p.norm_dist());

    let denom = norm0.cross(&norm1).dot(&norm2);

    if denom < f32::EPSILON as f64 {
        None
    } else {
        Some(
            (norm1.cross(&norm2) * dist0
                + norm2.cross(&norm0) * dist1
                + norm0.cross(&norm1) * dist2)
                / denom,
        )
    }
}

fn vertex_in_hull<'a>(mut planes: impl Iterator<Item = &'a Plane>, v: &'a Vec3<f64>) -> bool {
    !planes.any(|plane| {
        let (norm, dist) = plane.norm_dist();
        let proj = norm.dot(v);
        proj > dist && (dist - proj).abs() > 0.000009
    })
}

pub struct PolygonalMesh {
    pub vertices: Vec<Vec3<f64>>,
    pub normals: Vec<Vec3<f64>>,
    pub uvs: Vec<(f32, f32)>,
    pub surface_indices: Vec<u32>,
    pub indices: Vec<u32>,
}

impl PolygonalMesh {
    pub fn empty() -> Self {
        Self {
            vertices: vec![],
            normals: vec![],
            uvs: vec![],
            surface_indices: vec![],
            indices: vec![],
        }
    }

    pub fn from_brush(brush: &Brush, wad: &Wad) -> Self {
        let mut face_geometry = Vec::with_capacity(brush.planes.len());

        face_geometry.resize_with(brush.planes.len(), || vec![]);

        brush
            .planes
            .iter()
            .enumerate()
            .flat_map(|p1| repeat(p1).zip(brush.planes.iter()))
            .flat_map(|p1p2| repeat(p1p2).zip(brush.planes.iter()))
            .filter_map(|(((i, p1), p2), p3)| planes_common_point([p1, p2, p3]).map(|v| (i, v)))
            .filter(|(_, v)| vertex_in_hull(brush.planes.iter(), v))
            .for_each(|(i, v)| {
                if !face_geometry[i].iter().any(|(av, _, _, _)| av == &v) {
                    let plane = &brush.planes[i];
                    let texture = wad.mip_texture(&plane.texture_name.to_lowercase());
                    let texture_size = texture.map(|t| [t.width, t.height]).unwrap_or([64, 64]);
                    let texture_coordinates =
                        plane.texture_coordinates.get_uv(v, plane, texture_size);

                    face_geometry[i].push((v, texture_coordinates, plane.norm_dist().0, i as u32));
                }
            });

        let mut vertices = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];
        let mut surface_indices = vec![];
        let mut indices = vec![];

        for (i, face) in face_geometry.iter_mut().enumerate() {
            if face.len() < 3 {
                continue;
            }
            let face_basis = face[1].0 - face[0].0;
            let face_normal = brush.planes[i].norm_dist().0;
            let face_center = face
                .iter()
                .map(|(a, _, _, _)| a)
                .fold(Vec3::new(0.0, 0.0, 0.0), |a, b| a + *b)
                / face.len() as f64;
            face.sort_by(|(a, _, _, _), (b, _, _, _)| {
                let u = face_basis.normalize();
                let v = u.cross(&face_normal).normalize();

                let local_a = *a - face_center;
                let a_u = local_a.dot(&u);
                let a_v = local_a.dot(&v);

                let local_b = *b - face_center;
                let b_u = local_b.dot(&u);
                let b_v = local_b.dot(&v);

                let a_angle = a_v.atan2(a_u);
                let b_angle = b_v.atan2(b_u);

                a_angle.partial_cmp(&b_angle).unwrap()
            });

            let first_vert = vertices.len();

            vertices.extend(face.iter().map(|(a, _, _, _)| a));
            uvs.extend(face.iter().map(|(_, a, _, _)| a));
            normals.extend(face.iter().map(|(_, _, a, _)| a));
            surface_indices.extend(face.iter().map(|(_, _, _, a)| a));

            for index in 1..(face.len() - 1) {
                indices.push(first_vert as u32);
                indices.push((first_vert + index) as u32);
                indices.push((first_vert + index + 1) as u32);
            }
        }
        Self {
            vertices,
            normals,
            uvs,
            surface_indices,
            indices,
        }
    }

    pub fn write_obj<W: Write>(&self, w: &mut W) -> std::io::Result<()> {
        w.write("o Test\n".as_bytes())?;
        for v in self.vertices.iter() {
            w.write(format!("v {} {} {}\n", v.x as f32, v.y as f32, v.z as f32).as_bytes())?;
        }

        for (u, v) in self.uvs.iter() {
            w.write(format!("vt {} {}\n", u, v).as_bytes())?;
        }

        for is in self.indices.chunks_exact(3) {
            w.write(
                format!("f {}/{0} {}/{1} {}/{2}\n", is[0] + 1, is[1] + 1, is[2] + 1).as_bytes(),
            )?;
        }

        Ok(())
    }

    pub fn add_triangle(
        &mut self,
        vertices: [Vec3<f64>; 3],
        normals: [Vec3<f64>; 3],
        uvs: [(f32, f32); 3],
        surface_index: u32,
    ) {
        let start_i = self.vertices.len() as u32;
        self.vertices.extend(vertices);
        self.normals.extend(normals);
        self.uvs.extend(uvs);
        self.surface_indices.extend(repeat(surface_index).take(3));
        self.indices.extend(start_i..(start_i + 3));
    }

    pub fn partion_by_texture(&self, brush: &Brush) -> HashMap<String, PolygonalMesh> {
        let mut partitions = HashMap::new();

        for is in self.indices.chunks_exact(3) {
            let texture_name = brush.planes[self.surface_indices[is[0] as usize] as usize]
                .texture_name
                .clone();
            if !partitions.contains_key(&texture_name) {
                partitions.insert(texture_name.clone(), PolygonalMesh::empty());
            }
            let is = [is[0], is[1], is[2]];
            let vertices = is.map(|i| self.vertices[i as usize]);
            let normals = is.map(|i| self.normals[i as usize]);
            let uvs = is.map(|i| self.uvs[i as usize]);

            partitions.get_mut(&texture_name).unwrap().add_triangle(
                vertices,
                normals,
                uvs,
                self.surface_indices[is[0] as usize],
            );
        }

        partitions
    }

    pub fn merge(&mut self, other: &PolygonalMesh) {
        let first_vertex = self.vertices.len();
        self.vertices.extend_from_slice(&other.vertices);
        self.normals.extend_from_slice(&other.normals);
        self.uvs.extend_from_slice(&other.uvs);
        self.indices
            .extend(other.indices.iter().map(|i| i + first_vertex as u32));
    }
}

#[cfg(test)]
pub mod tests {
    use std::io::Cursor;

    use super::*;

    #[test]
    pub fn test() {
        let tests = include_str!("../test.map");
        let cursor = Cursor::new(tests);
        let parsed = crate::parser::parse(cursor).unwrap();
        //let mesh = PolygonalMesh::from_brush(&parsed.entities[1].brushes[0]);
        let wad_path = parsed
            .entities
            .iter()
            .map(|e| e.properties.get("wad"))
            .find(Option::is_some)
            .unwrap()
            .unwrap();
        let wad = Wad::load_from_path(wad_path).unwrap();
        let mut mesh = PolygonalMesh::empty();
        for entity in parsed.entities.iter() {
            if entity.brushes.len() == 0 {
                continue;
            }
            for brush in entity.brushes.iter() {
                mesh.merge(&PolygonalMesh::from_brush(brush, &wad));
            }
        }
        mesh.write_obj(&mut std::io::stdout()).unwrap();
    }
}
