use std::ops::{Add, Div, Mul, Sub};

pub trait Scalar: Clone + Copy + Add {}

impl<T: Clone + Copy + Add<Output = Self>> Scalar for T {}

pub trait Sqrt {
    fn sqrt(self) -> Self;
}

impl Sqrt for f32 {
    fn sqrt(self) -> Self {
        f32::sqrt(self)
    }
}

impl Sqrt for f64 {
    fn sqrt(self) -> Self {
        f64::sqrt(self)
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vec3<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl<
        T: Scalar
            + Mul<T, Output = T>
            + Add<Output = T>
            + Sub<T, Output = T>
            + Div<T, Output = T>
            + Sqrt,
    > Vec3<T>
{
    pub fn new(x: T, y: T, z: T) -> Self {
        Self { x, y, z }
    }

    pub fn dot(&self, rhs: &Self) -> T {
        self.x * rhs.x + self.y * rhs.y + self.z * rhs.z
    }

    pub fn cross(&self, rhs: &Self) -> Self {
        Self {
            x: self.y * rhs.z - self.z * rhs.y,
            y: self.z * rhs.x - self.x * rhs.z,
            z: self.x * rhs.y - self.y * rhs.x,
        }
    }

    pub fn length(&self) -> T {
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    pub fn normalize(&self) -> Self {
        self.clone() / self.length()
    }
}

impl<T: Scalar> From<&(T, T, T)> for Vec3<T> {
    fn from((x, y, z): &(T, T, T)) -> Self {
        let (x, y, z) = (*x, *y, *z);
        Self { x, y, z }
    }
}

impl From<&crate::parser::Point> for Vec3<f64> {
    fn from(crate::parser::Point { x, y, z }: &crate::parser::Point) -> Self {
        let (x, y, z) = (*x, *y, *z);
        Self { x, y, z }
    }
}

impl<T: Scalar + Add<Output = T>> Add for Vec3<T> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl<T: Scalar + Sub<Output = T>> Sub for Vec3<T> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl<T: Scalar + Mul<T, Output = T>> Mul<T> for Vec3<T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Self {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs,
        }
    }
}

impl<T: Scalar + Div<T, Output = T>> Div<T> for Vec3<T> {
    type Output = Self;

    fn div(self, rhs: T) -> Self::Output {
        Self {
            x: self.x / rhs,
            y: self.y / rhs,
            z: self.z / rhs,
        }
    }
}
