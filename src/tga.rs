use std::{
    io::Write,
    mem::{size_of, transmute},
};

#[repr(u8)]
#[allow(unused)]
enum TgaColorMapType {
    NoMap = 0,
    Present = 1,
}

#[repr(u8)]
#[allow(unused)]
enum TgaImageType {
    NoImage = 0,
    UncompressedColorMapped = 1,
    UncoppressedTrueColor = 2,
    UncompressedGrayscale = 3,
    RunlengthColorMapped = 9,
    RunlengthTrueColor = 10,
    RunlengthGrayscale = 11,
}

#[repr(packed)]
#[allow(unused)]
struct TgaColorMapInfo {
    pub _dummy: [u8; 5], //TODO
}

const IMAGE_DESC_LEFT_RIGHT: u8 = 1 << 4;

#[repr(packed)]
#[allow(unused)]
struct TgaImageInfo {
    pub x_origin: u16,
    pub y_origin: u16,
    pub width: u16,
    pub height: u16,
    pub pixel_depth: u8,
    pub image_desc: u8,
}

#[repr(packed)]
#[allow(unused)]
struct TgaHeader {
    pub id_length: u8,
    pub color_map_type: TgaColorMapType,
    pub image_type: TgaImageType,
    pub color_map_info: TgaColorMapInfo,
    pub image_info: TgaImageInfo,
}

pub fn store_simple_truecolor<W: Write>(
    stream: &mut W,
    width: u32,
    height: u32,
    data: &[u8],
) -> std::io::Result<()> {
    let header = TgaHeader {
        id_length: 0,
        color_map_type: TgaColorMapType::NoMap,
        image_type: TgaImageType::UncoppressedTrueColor,
        color_map_info: TgaColorMapInfo { _dummy: [0; 5] },
        image_info: TgaImageInfo {
            x_origin: 0,
            y_origin: 0,
            width: width as _,
            height: height as _,
            pixel_depth: 24,
            image_desc: IMAGE_DESC_LEFT_RIGHT,
        },
    };

    let header: [u8; size_of::<TgaHeader>()] = unsafe { transmute(header) };

    stream.write(&header)?;
    stream.write(data)?;

    Ok(())
}
