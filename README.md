rqmap
=======

About
-----------

This library has minimal dependencies and is meant to facilitate working with 
Quake's ```.map``` format.

It supports both standard and valve formats.

It can also load textures from ```WAD2``` files and contains a simple encoder
for the ```.obj``` and ```.tga``` formats.

Example
-----------

This crate contains an example that will convert a ```.map``` + ```.wad``` map
to ```.obj```, ```.mtl``` and ```.tga``` files

You can use the example converter by running the following command at the root
of this repo directory:

```
cargo run --example map2obj -- path_to_surce.map output_directory output_name
```

It will parse ```path_to_surce.map ``` and ouput the following files:

* ```output_directory/output_name.obj```
* ```output_directory/output_name.mtl```
* ```output_directory/texture_name.tga``` for each texture contained in the wad

