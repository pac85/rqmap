use std::{
    collections::HashMap,
    fmt::{Debug, Display},
    io::BufRead,
};

use thiserror::Error;

use crate::math::Vec3;

pub mod scanner;

#[derive(Error, Debug)]
pub struct GenericParserError {
    pub expected: scanner::TokenType,
    pub got: scanner::Token,
    pub line: u32,
    pub col: u32,
}

impl Display for GenericParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as Debug>::fmt(&self, f)
    }
}

#[derive(Error, Debug)]
pub enum ParseError {
    #[error(transparent)]
    GenericParserError(#[from] GenericParserError),
    #[error(transparent)]
    ScannerError(#[from] scanner::ScannerError),
    #[error(transparent)]
    IoError(#[from] std::io::Error),
}

fn consume<R: BufRead>(
    scanner: &mut scanner::PeekScanner<R>,
    expected_token: scanner::TokenType,
) -> Result<scanner::Token, ParseError> {
    let read_token = scanner.scan_token()?;
    if read_token.ty() != expected_token {
        Err(GenericParserError {
            expected: expected_token,
            got: read_token,
            line: scanner.line(),
            col: scanner.col(),
        }
        .into())
    } else {
        Ok(read_token)
    }
}

macro_rules! tmatch {
    ($scanner:expr, $expected_token:ident) => {
        (|| {
            let read_token = $scanner.scan_token()?;
            match read_token {
                scanner::Token::$expected_token(v) => Ok::<_, ParseError>(v),
                _ => Err(GenericParserError {
                    expected: scanner::TokenType::$expected_token,
                    got: read_token,
                    line: $scanner.line(),
                    col: $scanner.col(),
                }
                .into()),
            }
        })()
    };
}

pub trait Parse {
    fn parse<R: BufRead>(r: &mut scanner::PeekScanner<R>) -> Result<Self, ParseError>
    where
        Self: Sized;
}

#[derive(Debug)]
pub struct Point {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Parse for Point {
    fn parse<R: BufRead>(r: &mut scanner::PeekScanner<R>) -> Result<Self, ParseError>
    where
        Self: Sized,
    {
        consume(r, scanner::TokenType::LeftParen)?;
        let (x, y, z) = (
            tmatch!(r, Number)?,
            tmatch!(r, Number)?,
            tmatch!(r, Number)?,
        );
        consume(r, scanner::TokenType::RightParen)?;

        Ok(Self { x, y, z })
    }
}

#[derive(Debug)]
pub enum TextureCoordinates {
    Valve {
        tx1: f64,
        ty1: f64,
        tz1: f64,
        offset1: f64,
        tx2: f64,
        ty2: f64,
        tz2: f64,
        offset2: f64,
        rotation: f64,
        x_scale: f64,
        y_scale: f64,
    },
    Original {
        x_offset: f64,
        y_offset: f64,
        rotation: f64,
        x_scale: f64,
        y_scale: f64,
    },
}

fn dot_tuples((a1, a2): &(f64, f64), (b1, b2): &(f64, f64)) -> f64 {
    a1 * b1 + a2 * b2
}

fn deg_to_rad(deg: f64) -> f64 {
    deg * std::f64::consts::PI / 180.0
}

impl TextureCoordinates {
    pub fn get_uv(&self, v: Vec3<f64>, plane: &Plane, texture_size: [u32; 2]) -> (f32, f32) {
        match self {
            Self::Valve {
                tx1,
                ty1,
                tz1,
                offset1,
                tx2,
                ty2,
                tz2,
                offset2,
                rotation: _,
                x_scale,
                y_scale,
            } => {
                let u_axis = Vec3::new(*tx1, *ty1, *tz1);
                let v_axis = Vec3::new(*tx2, *ty2, *tz2);

                let (u, v) = (u_axis.dot(&v), v_axis.dot(&v));
                let (u, v) = (u / texture_size[0] as f64, v / texture_size[0] as f64);
                let (u, v) = (u / x_scale, v / y_scale);
                let (u, v) = (
                    u + offset1 / texture_size[0] as f64,
                    v + offset2 / texture_size[1] as f64,
                );

                (u as f32, v as f32)
            }
            Self::Original {
                x_offset,
                y_offset,
                rotation,
                x_scale,
                y_scale,
            } => {
                let (norm, _) = plane.norm_dist();
                let abs_norm = Vec3::new(norm.x.abs(), norm.y.abs(), norm.z.abs());
                let uv = if abs_norm.z >= abs_norm.y && abs_norm.z >= abs_norm.x {
                    (v.x, -v.y)
                } else if abs_norm.y >= abs_norm.z && abs_norm.y >= abs_norm.x {
                    (v.x, -v.z)
                } else
                /*if abs_norm.x >= abs_norm.z && abs_norm.x >= abs_norm.y*/
                {
                    (v.y, -v.z)
                };

                let rotation = deg_to_rad(*rotation);
                let (sin_rot, cos_rot) = rotation.sin_cos();
                let uv = (
                    dot_tuples(&uv, &(cos_rot, -sin_rot)),
                    dot_tuples(&uv, &(sin_rot, cos_rot)),
                );
                let uv = (uv.0 / texture_size[0] as f64, uv.1 / texture_size[1] as f64);
                let uv = (uv.0 / x_scale, uv.1 / y_scale);
                let uv = (
                    uv.0 + x_offset / texture_size[0] as f64,
                    uv.1 + y_offset / texture_size[1] as f64,
                );

                let (u, v) = uv;
                (u as f32, v as f32)
            }
        }
    }
}

#[derive(Debug)]
pub struct Plane {
    pub points: [Point; 3],
    pub texture_name: String,
    pub texture_coordinates: TextureCoordinates,
}

impl Plane {
    pub fn norm_dist(&self) -> (Vec3<f64>, f64) {
        let v0v1 = Vec3::from(&self.points[1]) - Vec3::from(&self.points[0]);
        let v1v2 = Vec3::from(&self.points[2]) - Vec3::from(&self.points[1]);

        let normal = v1v2.cross(&v0v1).normalize();
        let dist = Vec3::from(&self.points[0]).dot(&normal);

        (normal, dist)
    }
}

impl Parse for Plane {
    fn parse<R: BufRead>(r: &mut scanner::PeekScanner<R>) -> Result<Self, ParseError>
    where
        Self: Sized,
    {
        let points = [Point::parse(r)?, Point::parse(r)?, Point::parse(r)?];

        let texture_name = tmatch!(r, Texture)?;

        let texture_coordinates = if r.peek()?.ty() == scanner::TokenType::LeftBraket {
            //valve format
            consume(r, scanner::TokenType::LeftBraket)?;
            let tx1 = tmatch!(r, Number)?;
            let ty1 = tmatch!(r, Number)?;
            let tz1 = tmatch!(r, Number)?;
            let offset1 = tmatch!(r, Number)?;
            consume(r, scanner::TokenType::RightBraket)?;

            consume(r, scanner::TokenType::LeftBraket)?;
            let tx2 = tmatch!(r, Number)?;
            let ty2 = tmatch!(r, Number)?;
            let tz2 = tmatch!(r, Number)?;
            let offset2 = tmatch!(r, Number)?;
            consume(r, scanner::TokenType::RightBraket)?;

            let rotation = tmatch!(r, Number)?;
            let x_scale = tmatch!(r, Number)?;
            let y_scale = tmatch!(r, Number)?;

            TextureCoordinates::Valve {
                tx1,
                ty1,
                tz1,
                offset1,
                tx2,
                ty2,
                tz2,
                offset2,
                rotation,
                x_scale,
                y_scale,
            }
        } else {
            //valve format
            let x_offset = tmatch!(r, Number)?;
            let y_offset = tmatch!(r, Number)?;
            let rotation = tmatch!(r, Number)?;
            let x_scale = tmatch!(r, Number)?;
            let y_scale = tmatch!(r, Number)?;

            TextureCoordinates::Original {
                x_offset,
                y_offset,
                rotation,
                x_scale,
                y_scale,
            }
        };

        Ok(Self {
            points,
            texture_name,
            texture_coordinates,
        })
    }
}

#[derive(Debug)]
pub struct Brush {
    pub planes: Vec<Plane>,
}

impl Parse for Brush {
    fn parse<R: BufRead>(r: &mut scanner::PeekScanner<R>) -> Result<Self, ParseError>
    where
        Self: Sized,
    {
        let mut planes = vec![];
        while r.peek()?.ty() == scanner::TokenType::LeftParen {
            planes.push(Plane::parse(r)?);
        }
        Ok(Self { planes })
    }
}

#[derive(Debug)]
pub struct Entity {
    pub properties: HashMap<String, String>,
    pub brushes: Vec<Brush>,
}

impl Parse for Entity {
    fn parse<R: BufRead>(r: &mut scanner::PeekScanner<R>) -> Result<Self, ParseError>
    where
        Self: Sized,
    {
        let mut res = Self {
            properties: HashMap::new(),
            brushes: vec![],
        };
        while r.peek()?.ty() == scanner::TokenType::String {
            let k = tmatch!(r, String)?;
            let v = tmatch!(r, String)?;

            res.properties.insert(k, v);
        }

        while r.peek()?.ty() == scanner::TokenType::Comment
            || r.peek()?.ty() == scanner::TokenType::LeftBrace
        {
            if r.peek()?.ty() == scanner::TokenType::Comment {
                consume(r, scanner::TokenType::Comment)?;
            }
            consume(r, scanner::TokenType::LeftBrace)?;
            res.brushes.push(Brush::parse(r)?);
            consume(r, scanner::TokenType::RightBrace)?;
        }

        Ok(res)
    }
}

#[derive(Debug)]
pub struct Map {
    pub entities: Vec<Entity>,
}

impl Parse for Map {
    fn parse<R: BufRead>(r: &mut scanner::PeekScanner<R>) -> Result<Self, ParseError>
    where
        Self: Sized,
    {
        let mut entities = vec![];
        loop {
            match r.peek()? {
                scanner::Token::Comment(_) => {
                    consume(r, scanner::TokenType::Comment)?;
                }
                scanner::Token::LeftBrace => {
                    consume(r, scanner::TokenType::LeftBrace)?;
                    entities.push(Entity::parse(r)?);
                    consume(r, scanner::TokenType::RightBrace)?;
                }
                scanner::Token::Eof => {
                    break;
                }
                t => {
                    return Err(ParseError::GenericParserError(GenericParserError {
                        expected: scanner::TokenType::Comment,
                        got: t,
                        line: r.line(),
                        col: r.col(),
                    }));
                }
            }
        }
        Ok(Self { entities })
    }
}

pub fn parse<R: BufRead>(r: R) -> Result<Map, ParseError> {
    Map::parse(&mut scanner::PeekScanner::new(scanner::Scanner::new(r)))
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use super::*;

    #[test]
    fn test() {
        let tests = r##"
// Game: Quake
// Format: Valve
// entity 0
{
"classname" "worldspawn"
"mapversion" "220"
// brush 0
{
( -16 -64 -16 ) ( -16 -63 -16 ) ( -16 -64 -15 ) __TB_empty [ 0 -1 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
( -64 -16 -16 ) ( -64 -16 -15 ) ( -63 -16 -16 ) __TB_empty [ 1 0 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
( -64 -64 -16 ) ( -63 -64 -16 ) ( -64 -63 -16 ) __TB_empty [ -1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 64 64 16 ) ( 64 65 16 ) ( 65 64 16 ) __TB_empty [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 64 16 16 ) ( 65 16 16 ) ( 64 16 17 ) __TB_empty [ -1 0 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
( 16 64 16 ) ( 16 64 17 ) ( 16 65 16 ) __TB_empty [ 0 1 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
}
}
"##;
        let cursor = Cursor::new(tests);
        let parsed = Map::parse(&mut scanner::PeekScanner::new(scanner::Scanner::new(
            cursor,
        )))
        .unwrap();

        println!("{parsed:?}");
    }

    #[test]
    fn test1() {
        let tests = include_str!("../../test.map");
        let cursor = Cursor::new(tests);
        let parsed = Map::parse(&mut scanner::PeekScanner::new(scanner::Scanner::new(
            cursor,
        )))
        .unwrap();

        println!("{parsed:?}");
    }

    #[test]
    fn test2() {
        let tests = include_str!("../../E1M1.MAP");
        let cursor = Cursor::new(tests);
        let parsed = Map::parse(&mut scanner::PeekScanner::new(scanner::Scanner::new(
            cursor,
        )))
        .unwrap();

        println!("{parsed:?}");
    }
}
