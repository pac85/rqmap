use std::{fs::File, io::BufReader};

use rqmap::{obj::ObjEncoder, tga::store_simple_truecolor, MapAndWad};

fn main() {
    let (in_filename, out_path, out_name) = {
        let mut args = std::env::args();
        args.next().unwrap();
        (args.next().expect("\nUsage:\n    map2obj file.map\n"),
        args.next().expect("\nUsage:\n    map2obj file.map\n"),
        args.next().expect("\nUsage:\n    map2obj file.map\n"))
    };

    let stream = File::open(in_filename).expect("no such file");
    let mut stream = BufReader::new(stream);

    let map = MapAndWad::new(&mut stream).unwrap();
    let parts = map.partioned();

    for (name, texture) in map.wad().mip_textures().iter() {
        store_simple_truecolor(
            &mut File::create(out_path.to_owned() + "/" + name + ".tga").unwrap(),
            texture.width,
            texture.height,
            &texture.to_truecolor(0),
        )
        .unwrap();
    }

    let mut encoder = ObjEncoder::new(&out_path, &out_name).unwrap();
    for (texture, mesh) in parts {
        encoder.add_mesh(&texture, &mesh, &texture).unwrap();
    }
}
