use std::{
    fmt::{Debug, Display},
    io::BufRead,
};

use thiserror::Error;

#[derive(PartialEq, Debug, Clone)]
pub enum TokenType {
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    LeftBraket,
    RightBraket,
    String,
    Number,
    Comment,
    Texture,
    Newline,
    Blank,
    Eof,
}

#[derive(PartialEq, Debug, Clone)]
pub enum Token {
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    LeftBraket,
    RightBraket,
    String(String),
    Number(f64),
    Comment(String),
    Texture(String),
    Newline,
    Blank,
    Eof,
}

impl Token {
    pub fn ty(&self) -> TokenType {
        match self {
            Token::LeftParen => TokenType::LeftParen,
            Token::RightParen => TokenType::RightParen,
            Token::LeftBrace => TokenType::LeftBrace,
            Token::RightBrace => TokenType::RightBrace,
            Token::LeftBraket => TokenType::LeftBraket,
            Token::RightBraket => TokenType::RightBraket,
            Token::String(_) => TokenType::String,
            Token::Number(_) => TokenType::Number,
            Token::Comment(_) => TokenType::Comment,
            Token::Texture(_) => TokenType::Texture,
            Token::Newline => TokenType::Newline,
            Token::Blank => TokenType::Blank,
            Token::Eof => TokenType::Eof,
        }
    }
}

#[derive(Error, Debug)]
pub struct ErrorInfo {
    pub c: char,
    pub line: u32,
    pub col: u32,
}

impl Display for ErrorInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as Debug>::fmt(&self, f)
    }
}

#[derive(Error, Debug)]
pub enum ScannerError {
    //#[error(transparent)]
    //ScannerError(#[from] u32),
    //IoError(#[from] std::io::Error),
    #[error(transparent)]
    UnexpectedCharacter(#[from] ErrorInfo),
}

pub struct Scanner<R: BufRead> {
    stream: R,
    buf: String,
    current: usize,
    cline: u32,
}

impl<R: BufRead> Scanner<R> {
    pub fn new(stream: R) -> Self {
        Self {
            stream,
            buf: String::new(),
            current: 0,
            cline: 0,
        }
    }

    fn char_at(&self, i: usize) -> char {
        self.buf.as_bytes()[i] as char
    }

    fn advance(&mut self) -> char {
        if self.buf.len() <= self.current {
            self.cline += 1;
            self.current = 0;
            self.buf.clear();
            let res = self.stream.read_line(&mut self.buf).unwrap();
            //println!("scanning {} {}", self.cline, self.buf);
            if res == 0 {
                return '\0';
            }
        }
        self.current += 1;
        self.char_at(self.current - 1)
    }

    fn peek(&self) -> char {
        if self.buf.len() <= self.current {
            '\n'
        } else {
            self.char_at(self.current)
        }
    }

    fn cmatch(&mut self, c: char) -> bool {
        if self.buf.len() <= self.current && c == '\n' {
            self.advance();
            true
        } else if self.buf.len() <= self.current {
            false
        } else if self.char_at(self.current) == c {
            self.advance();
            true
        } else {
            false
        }
    }

    fn consume_until(&mut self, c: char) -> String {
        let mut s = String::new();
        while self.peek() != c {
            s.push(self.advance());
        }
        self.advance();
        s
    }

    fn string(&mut self) -> String {
        self.consume_until('"')
    }

    fn consume_line(&mut self) -> String {
        self.consume_until('\n')
    }

    fn number(&mut self) -> f64 {
        let start = self.current;
        while self.peek().is_numeric() {
            self.advance(); //TODO shouldn't go to new line
        }

        if self.peek() == '.' {
            self.advance();

            while self.peek().is_numeric() {
                self.advance(); //TODO shouldn't go to new line
            }
        }

        if self.peek() == 'e' {
            self.advance();

            if self.peek() == '-' {
                self.advance();
            }

            while self.peek().is_numeric() {
                self.advance(); //TODO shouldn't go to new line
            }
        }

        self.buf[start - 1..self.current].parse().unwrap()
    }

    fn texture(&mut self) -> String {
        let start = self.current;
        while self.peek() != ' ' {
            self.advance(); //TODO shouldn't go to new line
        }

        self.buf[start - 1..self.current].to_string()
    }

    fn scan_token_inner(&mut self) -> Result<Token, ScannerError> {
        Ok(match self.advance() {
            '(' => Token::LeftParen,
            ')' => Token::RightParen,
            '{' => Token::LeftBrace,
            '}' => Token::RightBrace,
            '[' => Token::LeftBraket,
            ']' => Token::RightBraket,
            '/' if self.cmatch('/') => Token::Comment(self.consume_line()),
            '"' => Token::String(self.string()),
            //'\n' => Token::Newline,
            '\n' | ' ' | '\r' | '\t' => Token::Blank,
            '\0' => Token::Eof,
            '-' | '0'..='9' => Token::Number(self.number()),
            c if c != ' ' => Token::Texture(self.texture()),
            c => {
                return Err(ScannerError::UnexpectedCharacter(ErrorInfo {
                    c,
                    line: self.line(),
                    col: self.col(),
                }))
            }
        })
    }

    pub fn scan_token(&mut self) -> Result<Token, ScannerError> {
        let mut scanned = Token::Blank;
        while scanned == Token::Blank || scanned == Token::Newline {
            scanned = self.scan_token_inner()?;
        }

        //println!("{scanned:?}");
        Ok(scanned)
    }

    pub fn line(&self) -> u32 {
        self.current as _
    }

    pub fn col(&self) -> u32 {
        self.cline
    }
}

pub struct PeekScanner<R: BufRead> {
    scanner: Scanner<R>,
    peeked_token: Option<Token>,
}

impl<R: BufRead> PeekScanner<R> {
    pub fn new(scanner: Scanner<R>) -> Self {
        Self {
            scanner,
            peeked_token: None,
        }
    }

    pub fn scan_token(&mut self) -> Result<Token, ScannerError> {
        match self.peeked_token.take() {
            Some(tok) => Ok(tok),
            None => Ok(self.scanner.scan_token()?),
        }
    }

    pub fn peek(&mut self) -> Result<Token, ScannerError> {
        if self.peeked_token.is_none() {
            self.peeked_token = Some(self.scanner.scan_token()?);
        }

        Ok(self.peeked_token.clone().unwrap())
    }

    pub fn line(&self) -> u32 {
        self.scanner.line()
    }

    pub fn col(&self) -> u32 {
        self.scanner.col()
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use super::*;

    #[test]
    pub fn test1() {
        let tests = r##"
// Game: Quake
// Format: Valve
// entity 0
{
"classname" "worldspawn"
"mapversion" "220"
// brush 0
{
( -16 -64 -16 ) ( -16 -63 -16 ) ( -16 -64 -15 ) __TB_empty [ 0 -1 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
( -64 -16 -16 ) ( -64 -16 -15 ) ( -63 -16 -16 ) __TB_empty [ 1 0 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
( -64 -64 -16 ) ( -63 -64 -16 ) ( -64 -63 -16 ) __TB_empty [ -1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 64 64 16 ) ( 64 65 16 ) ( 65 64 16 ) __TB_empty [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 64 16 16 ) ( 65 16 16 ) ( 64 16 17 ) __TB_empty [ -1 0 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
( 16 64 16 ) ( 16 64 17 ) ( 16 65 16 ) __TB_empty [ 0 1 0 -0 ] [ 0 0 -1 -0 ] -0 1 1
}
}
"##;
        let stream = Cursor::new(tests);
        let mut scanner = Scanner::new(stream);
        loop {
            let token = scanner.scan_token().unwrap();

            println!("{token:?}");

            if token == Token::Eof {
                break;
            }
        }
    }
}
