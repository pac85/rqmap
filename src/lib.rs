use std::{collections::HashMap, io::BufRead};

use geometry::PolygonalMesh;
use parser::{Map, ParseError};
use wad::Wad;

pub mod geometry;
pub mod math;
pub mod obj;
pub mod parser;
pub mod tga;
pub mod wad;

pub struct MapAndWad {
    parsed: Map,
    wad: Wad,
}

impl MapAndWad {
    pub fn new<R: BufRead>(stream: &mut R) -> Result<Self, ParseError> {
        let parsed = crate::parser::parse(stream)?;
        let wad_path = parsed
            .entities
            .iter()
            .map(|e| e.properties.get("wad"))
            .find(Option::is_some)
            .unwrap()
            .unwrap();
        let wad = Wad::load_from_path(wad_path)?;

        Ok(Self { parsed, wad })
    }

    pub fn partioned(&self) -> HashMap<String, PolygonalMesh> {
        let mut meshes = HashMap::new();
        for entity in self.parsed.entities.iter() {
            if entity.brushes.len() == 0 {
                continue;
            }
            for brush in entity.brushes.iter() {
                for (texture_name, mesh) in PolygonalMesh::from_brush(brush, &self.wad)
                    .partion_by_texture(brush)
                    .iter()
                {
                    if !meshes.contains_key(texture_name) {
                        meshes.insert(texture_name.clone(), PolygonalMesh::empty());
                    }
                    meshes.get_mut(texture_name).unwrap().merge(mesh);
                }
            }
        }

        meshes
    }

    pub fn map(&self) -> &Map {
        &self.parsed
    }

    pub fn wad(&self) -> &Wad {
        &self.wad
    }
}
