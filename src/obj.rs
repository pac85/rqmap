use std::{fs::File, io::Write};

use crate::geometry::PolygonalMesh;

pub struct ObjEncoder {
    stream: File,
    stream_mtl: File,
    vertices_so_far: u32,
}

impl ObjEncoder {
    pub fn new(path: &str, name: &str) -> std::io::Result<Self> {
        let mut stream = File::create(path.to_owned() + "/" + name + ".obj")?;
        let stream_mtl = File::create(path.to_owned() + "/" + name + ".mtl")?;

        stream.write(format!("mtllib {name}.mtl\n").as_bytes())?;

        Ok(Self {
            stream,
            stream_mtl,
            vertices_so_far: 0,
        })
    }

    pub fn add_mesh(
        &mut self,
        name: &str,
        mesh: &PolygonalMesh,
        texture: &str,
    ) -> std::io::Result<()> {
        self.stream.write(format!("o {}\n", name).as_bytes())?;
        for v in mesh.vertices.iter() {
            self.stream
                .write(format!("v {} {} {}\n", v.x as f32, v.y as f32, v.z as f32).as_bytes())?;
        }

        for (u, v) in mesh.uvs.iter() {
            self.stream.write(format!("vt {} {}\n", u, v).as_bytes())?;
        }

        self.stream
            .write(format!("usemtl {texture}\ns off\n").as_bytes())?;

        for is in mesh.indices.chunks_exact(3) {
            self.stream.write(
                format!(
                    "f {}/{0} {}/{1} {}/{2}\n",
                    self.vertices_so_far + is[0] + 1,
                    self.vertices_so_far + is[1] + 1,
                    self.vertices_so_far + is[2] + 1
                )
                .as_bytes(),
            )?;
        }

        self.vertices_so_far += mesh.vertices.len() as u32;

        self.stream_mtl.write(
            format!(
                r##"
newmtl {}
Ns 250.000000
Ka 1.000000 1.000000 1.000000
Kd 0.800000 0.800000 0.800000
Ks 0.000000 0.000000 0.000000
Ke 0.000000 0.000000 0.000000
Ni 1.450000
d 1.000000
illum 2
"##,
                texture
            )
            .as_bytes(),
        )?;

        self.stream_mtl
            .write(format!("map_Kd {}.tga\n", texture).as_bytes())?;

        Ok(())
    }
}
